# pre-commit-hooks

Gitlab hook implementation using [pre-commit](https://pre-commit.com/) instead of lefthook.

## Installation

Install `pre-commit`

```console
$ pip install pre-commit
```

## Usage

Add following file inside gitlab repo folder `.pre-commit-config.yaml`:

```yaml
default_stages:
  - commit

repos:
  - repo: https://gitlab.com/acunskis/pre-commit-hooks
    rev: 0.0.5
    hooks:
      - id: rubocop
      - id: haml-lint
      - id: eslint
      - id: prettier
      - id: yamllint
      - id: danger
        stages: [push]
```

Install hooks:

```console
$ pre-commit install -t pre-commit -t pre-push
```
